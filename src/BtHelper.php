<?php
declare(strict_types=1);

namespace annon;

/**
 * @property string $BT_PANEL
 * @property string $BT_KEY
 * Class BtHelper
 * @package annon
 */
class BtHelper
{
    private $BT_KEY = "";
    private $BT_PANEL = "";
    private $action = [];
    private $data = [];

    /**
     * 设置面板地址以及面板API密钥
     * BtHelper constructor.
     * @param string $BT_PANEL
     * @param string $BT_KEY
     */
    public function __construct(string $BT_PANEL, string $BT_KEY)
    {
        $this->BT_KEY = $BT_KEY;
        $this->BT_PANEL = $BT_PANEL;
        $this->data = $this->GetKeyData();
    }

    /**
     * 指定访问的控制器和方法
     * @param string $controller
     * @param string $method
     * @return $this
     */
    public function action(string $controller, string $method): BtHelper
    {
        $this->action['controller'] = $controller;
        $this->action['method'] = $method;
        return $this;
    }

    /**
     * 设置要传输的参数
     * @param array $data
     * @return $this
     */
    public function data(array $data): BtHelper
    {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        $url = "{$this->BT_PANEL}/{$this->action['controller']}?action={$this->action['method']}";
        $result = $this->HttpPostCookie($url, $this->data);
        return json_decode($result, true);
    }

    /**
     * 构造带有签名的关联数组
     */
    private function GetKeyData(): array
    {
        $now_time = time();
        return [
            'request_token' => md5($now_time . '' . md5($this->BT_KEY)),
            'request_time' => $now_time
        ];
    }

    /**
     * 发起POST请求
     * @param string $url 目标网填，带http://
     * @param array|string $data 欲提交的数据
     * @param int $timeout
     * @return string
     */
    private function HttpPostCookie(string $url, $data, int $timeout = 60): string
    {
        $cookie_file = __DIR__ . DIRECTORY_SEPARATOR . md5($this->BT_PANEL) . '.cookie.txt';
        if (!file_exists($cookie_file)) {
            $fp = fopen($cookie_file, 'w+');
            fclose($fp);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}