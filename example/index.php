<?php

require_once __DIR__ . "/../src/BtHelper.php";

$url = "xxx"; // 面板地址
$key = "xxxx"; // 面板API密钥

$bt = new \annon\BtHelper($url, $key);

$data = [
    'table' => 'logs',
    'limit' => 10,
    'tojs' => 'test'
];

$data = $bt->action('data', 'getData')
    ->data($data)
    ->run();

var_dump($data);
